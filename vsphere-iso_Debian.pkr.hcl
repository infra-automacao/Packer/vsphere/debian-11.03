source "vsphere-iso" "this" {
  vcenter_server    = var.vsphere_server
  username          = var.vsphere_user
  password          = var.vsphere_password
  datacenter        = var.datacenter
  cluster           = var.cluster
  insecure_connection  = true
  http_directory = "http"

  vm_name = "brtec-Debian-11.03"
  guest_os_type = "debian10_64guest"

  ssh_username = "brtec"
  ssh_password = "Adm3843zu"

  CPUs =             4
  RAM =              4000
  RAM_reserve_all = true

  disk_controller_type =  ["lsilogic-sas"]
  datastore = var.datastore
  storage {
    disk_size =        30000
    disk_thin_provisioned = true
  }

  iso_paths = ["[datastore1] Iso/debian-11.3.0-amd64-netinst.iso"]
  // iso_checksum = "sha256:b23488689e16cad7a269eb2d3a3bf725d3457ee6b0868e00c8762d3816e25848"

  network_adapters {
    network =  var.network_name
    network_card = "vmxnet3"
  }


  boot_command = [
        "<esc><wait>",
        "install <wait>",
        " preseed/url=http://{{ .HTTPIP }}:{{ .HTTPPort }}/preseed.cfg<wait>",
        " auto-install/enable=true ",
        " locale=en_US.UTF-8 <wait>",
        " priority=critical",
        "<enter><wait>"
  ]
}

build {
  sources  = [
    "source.vsphere-iso.this"
  ]

  provisioner "shell-local" {
    inline  = ["echo the address is: $PACKER_HTTP_ADDR and build name is: $PACKER_BUILD_NAME"]
  }
}
