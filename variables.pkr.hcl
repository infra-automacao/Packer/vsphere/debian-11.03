variable "vsphere_server" {
  type    = string
  default = "ha-datacenter"
}

variable "vsphere_user" {
  type    = string
  default = "root"
}

variable "vsphere_password" {
  type    = string
  default = "password"
}

variable "datacenter" {
  type    = string
  default = "Virtual Machines"
}

variable "cluster" {
  type    = string
  default = ""
}

variable "datastore" {
  type    = string
  default = "datastore1"
}

variable "network_name" {
  type    = string
  default = "VM Network"
}
